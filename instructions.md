# Run in the cmd to build the image:
```
docker build -t hello .
```
# Run in the cmd to run the container:
```
docker run -p 8080:8080 hello
```
